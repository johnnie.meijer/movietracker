using TMDbLib.Client;

// Update database 
public class UpdateMovieDatabase 
{
    // API Key for TMDB API
    private readonly string API_KEY = "2ed66600fef7a37ebbe1601cfadea528";
    // Database
    private readonly MovieTrackerContext _database;
    private readonly TMDbClient _client;

    // Constructor
    public UpdateMovieDatabase(
        MovieTrackerContext database, 
        TMDbClient client)
    {
        _database = database;
        _client = client;
    }

    // Update database with director for all movies
    private void AddDirectorToMovies()
    {
        var movies = _database.Movies.ToList();

        foreach (var movie in movies)
        {
            var director = getDirector(movie.Id);
            movie.Director = director; 
        }

        _database.SaveChangesAsync();
    }

    // Get Director for a movie
    private string getDirector(int movieId)
    {
        var credits = _client.GetMovieCreditsAsync(movieId).Result;
        var director = credits.Crew.FirstOrDefault(c => c.Job == "Director")?.Name;

        if (director != null)
        {
            return director;
        }
        
        return "Unknown";
    }

    // Update database with cast for all movies
    public void AddCastToMovies()
    {
        var movies = _database.Movies.ToList();

        foreach (var movie in movies)
        {
            var cast = getCast(movie.Id);
            movie.Cast = cast;
        }

        _database.SaveChangesAsync();
    }

    // get cast for a movie
    private List<CastMember> getCast(int movieId)
    {
        var credits = _client.GetMovieCreditsAsync(movieId).Result;
        var cast = credits.Cast.Take(10).Select(c => new CastMember
        {
            ActorName = c.Name,
            Character = c.Character,
            MovieId = movieId
        }).ToList();

        return cast;
    }

    // Update database with genres in the Genre table
    public void AddGenresToDatabase()
    {
        var genres = _client.GetMovieGenresAsync().Result;
        var genreList = genres.Select(g => new Genre
        {
            Id = g.Id,
            Name = g.Name
        }).ToList();

        _database.Genres.AddRange(genreList);
        _database.SaveChangesAsync();
    }

    // Update database with genres for all movies and save to MovieGenre table
    public void AddGenresToMovies()
    {
        var movies = _database.Movies.ToList();

        foreach (var movie in movies)
        {
            var genres = getGenres(movie.Id);

            foreach (var genre in genres)
            {
                var movieGenre = new MovieGenre
                {
                    MovieId = movie.Id,
                    GenreId = genre.Id
                };

                if (!_database.MovieGenres.Any(mg => mg.MovieId == movieGenre.MovieId && mg.GenreId == movieGenre.GenreId))
                {
                    _database.MovieGenres.Add(movieGenre);
                }
            }
        }

        _database.SaveChangesAsync();
    }

    private List<Genre> getGenres(int movieId)
    {
        var genres = _client.GetMovieAsync(movieId).Result.Genres;
        var genreList = genres.Select(g => new Genre
        {
            Id = g.Id,
            Name = g.Name
        }).ToList();

        return genreList;
    }
}