using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using TMDbLib.Client;
using SearchMovie = TMDbLib.Objects.Search.SearchMovie;

namespace MovieTracker.Controllers;

// The MovieController class. 
[ApiController]
[Route("[controller]")]
public class MovieController : ControllerBase
{
    private readonly string TMDB_BASE_URL = "https://image.tmdb.org/t/p/original/";
    private readonly string NO_DESCRIPTION = "No description available";
    private readonly string NO_POSTER = "/assets/NoPoster.jpg";
    private readonly string NO_TITLE = "No title found";
    private readonly ILogger<MovieController> _logger;
    private readonly MovieTrackerContext _database;
    private readonly TMDbClient _client;

    // Constructor
    public MovieController(
        ILogger<MovieController> logger, 
        MovieTrackerContext database,
        TMDbClient client)
    {
        _logger = logger;
        _database = database;
        _client = client;
    }

    // The GET function to return a list of movies
    [HttpGet]
    public IEnumerable<Movie> Get(string movieTitle)
    {   
        // Get all results from the movieTitle
        var results = _client.SearchMovieAsync(movieTitle).Result.Results;

        // Set movie details to the Movie model if a movie is found
        var movieList = new List<Movie>();
        if (results != null)
        {
            foreach (var result in results)
            {
                var foundMovie = SetMovieDetails(result);
                movieList.Add(foundMovie);
            }
            return movieList;
        }
        else
        {
            return movieList;
        }
    }

    // The GET function to return a list of all movies from the global movie library
    [HttpGet("library")]
    public IEnumerable<Movie> GetLibrary()
    {
        // Read JSON file, deserialize and convert to List of movies
        var movieLibrary = _database.Movies.ToList();

        return movieLibrary;
    }

    // The GET function to return a list of movies from the user's library
    [HttpGet("userLibrary")]
    public async Task<ActionResult<List<MovieDTO>>> GetUserLibrary(string username)
    {
        // Get user from database and include UserMovies, Movies, Cast and Genre(s) in the result
        var user = await _database.Users
            .Include(u => u.UserMovies)
            .ThenInclude(um => um.Movie)
            .ThenInclude(m => m.Cast)
            .Include(u => u.UserMovies)
            .ThenInclude(um => um.Movie)
            .ThenInclude(m => m.MovieGenres)
            .ThenInclude(mg => mg.Genre)
            .FirstOrDefaultAsync(u => u.UserName == username);

        if (user == null)
        {
            return NotFound($"User with username {username} not found.");
        }

        // Get all movies from the user's library
        var result = user.UserMovies.Select(um => new MovieDTO
        {
            Title = um.Movie.Title,
            Id = um.Movie.Id,
            PosterUrl = um.Movie.PosterUrl,
            TmdbRating = um.Movie.TmdbRating,
            PersonalRating = um.PersonalRating,
            Director = um.Movie.Director,
            Watched = um.Watched,
            Description = um.Movie.Description,
            Release = um.Movie.Release,
            CreatedAt = um.Movie.CreatedAt,
            UpdatedAt = um.Movie.UpdatedAt,
            Cast = um.Movie.Cast.Select(c => new CastMemberDTO
            {
                Id = c.Id,
                Character = c.Character,
                ActorName = c.ActorName,
            }).ToList(),
            Genres = um.Movie.MovieGenres.Select(mg => new GenreDTO
            {
                Id = mg.Genre.Id,
                Name = mg.Genre.Name,
            }).ToList()
        }).ToList();

        return result;
    }

    // The DELETE function to remove a movie from the user's library
    [HttpDelete("{username}/library/{movieId}")]
    public async Task<IActionResult> DeleteMovieFromUserLibrary(string username, int movieId)
    {
        // Get user from database and include UserMovies and Movies in the result
        var user = await _database.Users
            .Include(u => u.UserMovies)
            .ThenInclude(um => um.Movie)
            .FirstOrDefaultAsync(u => u.UserName == username);

        if (user == null)
        {
            return NotFound($"User with username {username} not found.");
        }

        // Get all movies from the user's library
        var userMovieLibrary = user.UserMovies.Select(um => um.Movie).ToList();

        // Get the movie from the user's library
        var movie = userMovieLibrary.FirstOrDefault(m => m.Id == movieId);

        if (movie == null)
        {
            return NotFound($"Movie with id {movieId} not found in user's library.");
        }

        // Remove the movie from the user's library and save changes to database
        var userMovie = user.UserMovies.FirstOrDefault(um => um.MovieId == movieId);
        if (userMovie != null)
        {
            user.UserMovies.Remove(userMovie);
        }

        await _database.SaveChangesAsync();

        return Ok();
    }

    // The POST function to ADD a movie from searchresults to the user's Movie Library
    [HttpPost("userMovie")]
    public async Task<IActionResult> Post(UserMovieRequest request)
    {
        // Check if User exists and get UserId
        var existingUser = await _database.Users.FirstOrDefaultAsync(u => u.UserName == request.UserName);
        if (existingUser == null)
        {
            return BadRequest("User cannot be found");
        }
        var userId = existingUser.Id;

        // Check if Movie != null and if movie exists in global library. If not, add it. 
        if (request.Movie == null)
        {
            return BadRequest("Movie cannot be found");
        }

        var existingMovie = await _database.Movies.FirstOrDefaultAsync(m => m.Id == request.Movie.Id);
        if (existingMovie == null)
        {
            existingMovie = await AddMovieToGlobalLibrary(request.Movie);
        }

        var userMovie = await AddMovieToUserLibrary(existingMovie, existingUser);
        if (userMovie == null)
        {
            return BadRequest("Movie already exists in user's library");
        }

        // Convert to MovieResponseDTO
        var convertedMovie = ConvertToMovieResponseDTO(existingMovie);
        // Return result
        return Ok(convertedMovie);
    }

    // Converts a Movie to a MovieResponseDTO
    private MovieResponseDTO ConvertToMovieResponseDTO(Movie existingMovie)
    {
        return new MovieResponseDTO
        {
            Title = existingMovie.Title,
            Id = existingMovie.Id,
            Director = existingMovie.Director,
            Release = existingMovie.Release,
        };
    }

    // The PUT function to UPDATE the personalRating of a movie in the Movie Library
    [HttpPut("{username}/library/{movieId}")]
    public async Task<IActionResult> UpdatePersonalRating(string username, int movieId, double personalRating)
    {
        var user = await _database.Users.FirstOrDefaultAsync(u => u.UserName == username);
        if (user == null)
        {
            return NotFound($"User with username {username} not found.");
        }

        var userMovie = await _database.UserMovies.FirstOrDefaultAsync(um => um.UserId == user.Id && um.MovieId == movieId);
        if (userMovie == null)
        {
            return NotFound($"Movie with id {movieId} not found in user's library.");
        }

        userMovie.PersonalRating = personalRating;
        userMovie.UpdatedAt = DateTime.Now;

        await _database.SaveChangesAsync();

        return Ok();
    }

    // The PUT function to UPDATE the watched status of a movie in the Movie Library
    [HttpPut("{username}/library/{movieId}/watched")]
    public async Task<IActionResult> UpdateWatchedStatus(string username, int movieId, bool watched)
    {
        var user = await _database.Users.FirstOrDefaultAsync(u => u.UserName == username);
        if (user == null)
        {
            return NotFound($"User with username {username} not found.");
        }

        var userMovie = await _database.UserMovies.FirstOrDefaultAsync(um => um.UserId == user.Id && um.MovieId == movieId);
        if (userMovie == null)
        {
            return NotFound($"Movie with id {movieId} not found in user's library.");
        }

        userMovie.Watched = watched;
        userMovie.UpdatedAt = DateTime.Now; 

        await _database.SaveChangesAsync();

        return Ok();
    }

    // Creates a new UserMovie object
    private UserMovie createUserMovie(Movie movie, string userId)
    {
        return new UserMovie
        {
            UserId = userId,
            MovieId = movie.Id,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now
        };
    }

    // Sets the correct movie details from the TMDB movie to the MovieTracker Movie model. 
    private Movie SetMovieDetails(SearchMovie movieResults)
    {
        // Year of release is 0 when ReleaseDate is null
        int yearOfRelease = 0;
        if (movieResults.ReleaseDate != null)
        {
            yearOfRelease = movieResults.ReleaseDate!.Value.Year;
        }
        else
        {
            yearOfRelease = 0;
        }

        // Use poster from local assets when no poster is available
        string posterPath = String.Empty;
        if (movieResults.PosterPath != null)
        {
            posterPath = $"{TMDB_BASE_URL}{movieResults.PosterPath}";
        }
        else
        {
            posterPath = NO_POSTER;
        }

        // Return a new movie with the correct properties
        return new Movie
        {
            Title = movieResults.Title ?? NO_TITLE,
            Id = movieResults.Id,
            PosterUrl = posterPath,
            TmdbRating = movieResults.VoteAverage,
            Description = movieResults.Overview ?? NO_DESCRIPTION,
            Release = yearOfRelease
        };
    }

    // Add a movie to the UserMovieLibrary
    private async Task<UserMovie?> AddMovieToUserLibrary(Movie movie, User existingUser)
    {
        var existingUserMovie = await _database.UserMovies.FirstOrDefaultAsync(um => um.MovieId == movie.Id && um.UserId == existingUser.Id);
        if (existingUserMovie != null)
        {
            return null;
        }

        // Add Movie to User's library
        var userMovie = createUserMovie(movie, existingUser.Id);
        _database.UserMovies.Add(userMovie);
        await _database.SaveChangesAsync();

        // Remove navigation properties to avoid circular reference
        movie.UserMovies.Clear();
        existingUser.UserMovies.Clear();

        return userMovie;
    }

    // Add a movie to the global movie library
    private async Task<Movie> AddMovieToGlobalLibrary(Movie movie)
    {
        // Get credits and add director to movie
        var credits = _client.GetMovieCreditsAsync(movie.Id).Result;
        movie.Director = credits.Crew.FirstOrDefault(c => c.Job == "Director")?.Name;

        // Add DateTimes to movie
        movie.CreatedAt = DateTime.Now;
        movie.UpdatedAt = DateTime.Now;

        _database.Movies.Add(movie);
        await _database.SaveChangesAsync();

        // Add cast to movie
        movie.Cast = credits.Cast.Take(10).Select(c => new CastMember
        {
            ActorName = c.Name,
            Character = c.Character,
            MovieId = movie.Id
        }).ToList();

        _database.CastMembers.AddRange(movie.Cast);
        await _database.SaveChangesAsync();
        
        await AddGenresAsync(movie);

        return movie;
    }

    private async Task AddGenresAsync(Movie movie)
    {
        // Add genre(s) to movie
        var genres = await _client.GetMovieAsync(movie.Id);
        var genreList = genres.Genres.Select(g => new Genre
        {
            Id = g.Id,
            Name = g.Name
        }).ToList();

        foreach (var genre in genreList)
        {
            var movieGenre = new MovieGenre
            {
                MovieId = movie.Id,
                GenreId = genre.Id
            };

            if (!_database.MovieGenres.Any(mg => mg.MovieId == movieGenre.MovieId && mg.GenreId == movieGenre.GenreId))
            {
                _database.MovieGenres.Add(movieGenre);
            }
        }

        await _database.SaveChangesAsync();
    }
}
