using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MovieTracker.Models;

namespace MovieTracker.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly UserManager<User> _userManager;
    private readonly SignInManager<User> _signInManager;
    private readonly MovieTrackerContext _database;
    private readonly ILogger<UserController> _logger;
    private readonly JwtSettings _jwtSettings;

    public UserController(
        UserManager<User> userManager,
        SignInManager<User> signInManager,
        MovieTrackerContext database,
        IOptions<JwtSettings> jwtSettings,
        ILogger<UserController> logger)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _database = database;
        _jwtSettings = jwtSettings.Value;
        _logger = logger;
    }

    [HttpPost]
    public async Task<IActionResult> Post(RegisterViewModel userModel)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        // Check if userName already exists in the database
        var existingUser = _database.Users.FirstOrDefault(u => u.UserName == userModel.UserName);
        if (existingUser != null)
        {
            return Conflict(new { message = "User already exists" });
        }

        var user = SetUserProperties(userModel);

        var result = await _userManager.CreateAsync(user, user.PasswordHash);

        if (!result.Succeeded)
        {
            return BadRequest(result.Errors);
        }

        _logger.LogInformation("User created successfully: {UserName}", user.UserName);

        await _signInManager.SignInAsync(user, isPersistent: true);
        return Ok();
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginModel loginModel)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var user = await _userManager.FindByNameAsync(loginModel.UserName);
        if (user == null)
        {
            return Unauthorized(new { message = "Username does not exist" });
        }

        var result = await _signInManager.CheckPasswordSignInAsync(user, loginModel.Password, false);

        if (!result.Succeeded)
        {
            return Unauthorized(new { message = "That's an incorrect password" });
        }

        _logger.LogInformation($"User logged in successfully: {loginModel.UserName}");

        var token = GenerateJwtToken(user);

        return Ok(new { token, username = user.UserName } );
    }

    private string GenerateJwtToken(User user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_jwtSettings.SecretKey);
        var claims = new List<Claim>
    {
        new Claim(ClaimTypes.NameIdentifier, user.Id),
        new Claim(ClaimTypes.Name, user.UserName),
    };

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.AddMinutes(_jwtSettings.ExpirationInMinutes),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            Issuer = _jwtSettings.Issuer,
            Audience = _jwtSettings.Audience
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    private User SetUserProperties(RegisterViewModel userModel)
    {
        var user = new User()
        {
            UserName = userModel.UserName,
            PasswordHash = userModel.Password,
            FirstName = string.Empty,
            LastName = string.Empty,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now
        };

        return user;
    }
}