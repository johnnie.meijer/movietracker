import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NgMovie } from '../movie-library/movie-library.component';

@Component({
  selector: 'app-movie-details-dialog',
  templateUrl: './movie-details-dialog.component.html',
  styleUrls: ['./movie-details-dialog.component.css']
})
export class MovieDetailsDialogComponent {
  expanded: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: NgMovie,
    public dialogRef: MatDialogRef<MovieDetailsDialogComponent>,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
