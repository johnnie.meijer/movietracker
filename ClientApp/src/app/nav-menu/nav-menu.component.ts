import { Component, ViewChild } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  modalRef!: NgbModalRef;

  @ViewChild('logoutConfirmationModal') logoutConfirmationModal: any;

  constructor(
    public authService: AuthService,
    private modalService: NgbModal,
    ) { }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  openLogoutConfirmationModal() {
    this.modalRef = this.modalService.open(this.logoutConfirmationModal, { centered: true, size: 'sm' });
  }
}
