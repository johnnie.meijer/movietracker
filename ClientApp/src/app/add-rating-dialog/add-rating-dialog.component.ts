import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface RatingDialogData {
  movieTitle: string;
}

@Component({
  selector: 'app-add-rating-dialog',
  templateUrl: './add-rating-dialog.component.html',
  styleUrls: ['./add-rating-dialog.component.css']
})
export class AddRatingDialogComponent implements OnInit {

  ratingForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddRatingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RatingDialogData,
    private fb: FormBuilder
  ) {
    this.ratingForm = this.fb.group({
      rating: [5, [Validators.required, Validators.min(1), Validators.max(10)]]
    });
  }

  ngOnInit(): void {
    this.ratingForm = this.fb.group({
      rating: [5], // Set the initial value for the rating slider
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    const ratingValue = this.ratingForm.get('rating')?.value;
    this.dialogRef.close(ratingValue);
  }
}
