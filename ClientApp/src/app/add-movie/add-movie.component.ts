import { HttpClient } from "@angular/common/http";
import { Component, Inject } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { NgMovie } from "../movie-library/movie-library.component";
import { AuthService } from "../services/auth.service";

@Component({
    selector: 'app-add-movie',
    templateUrl: './add-movie.component.html',
    styleUrls: ['./add-movie.component.css']
})


export class AddMovieComponent {
    imageWidth = 200;
    imageMargin = 2;
    title = '';
    username = '';
    isLoading: boolean = false;

    public searchResults: NgMovie[] = [];

    constructor(
        private http: HttpClient,
        @Inject('BASE_URL') private baseUrl: string,
        private snackBar: MatSnackBar,
        public authService: AuthService,
    ) { }

    searchForMovies(title: string) {
        this.isLoading = true;
        this.http.get<NgMovie[]>(this.baseUrl + 'movie' + `?movieTitle=${title}`).subscribe(
            result => {
                this.searchResults = result;
                this.isLoading = false;
            },
            error => {
                console.error(error);
                this.isLoading = false;
            }
        );
    }

    addToLibrary(movie: NgMovie) {
        const requestBody = {
            username: this.authService.getUsername(),
            movie: movie,
        };

        this.http.post<NgMovie>(this.baseUrl + 'movie/userMovie', requestBody).subscribe(
            result => {
                this.blueLog(`${result.title.toLocaleUpperCase()} added to library`);
                this.showSnackbar('Successfully added ' + movie.title.toLocaleUpperCase() + ' to your library.', 'success-snackbar');
            },
            error => {
                console.error(error);
                this.info(`Did not add ${movie.title.toLocaleUpperCase()}. ${error.error}`);
                this.showSnackbar(error.error, 'error-snackbar');
            });
    }

    showSnackbar(message: string, styling: string) {
        this.snackBar.open(message, 'OK', {
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: styling,
        });
    }

    info(message: string) {
        console.info('%c' + message, 'color: gold');
    }

    blueLog(message: string) {
        console.log('%c' + message, 'color: #8dc6ff');
    }
}



