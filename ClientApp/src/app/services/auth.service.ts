import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { BehaviorSubject, Observable, tap } from "rxjs";
import { User } from "../models/user.model";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
// The AuthService class is responsible for handling user authentication and authorization.
export class AuthService {

    private isAuthenticatedSubject = new BehaviorSubject<boolean>(false);
    private usernameSubject = new BehaviorSubject<string>('');

    // The constructor() method injects the HttpClient, Router, and MatSnackBar services.
    constructor(
        private http: HttpClient,
        private router: Router,
        private snackBar: MatSnackBar,
        @Inject('BASE_URL') private baseUrl: string) 
        {
            this.checkLocalStoragetoken();
        }

    // The isAuthenticated() method returns the value of the isAuthenticatedSubject BehaviorSubject.
    public isAuthenticated(): boolean {
        return this.isAuthenticatedSubject.value;
    }

    // The getUsername() method returns the value of the usernameSubject BehaviorSubject.
    public getUsername(): string {
        return this.usernameSubject.value;
    }

    // The register() method sends a POST request to the server to register a new user.
    public register(user: User): Observable<any> {
        return this.http.post(this.baseUrl + 'user', user);
    }

    // The login() method sends a POST request to the server to log in a user.
    public login(username: string, password: string): Observable<any> {
        return this.http.post<LoginResponse>(this.baseUrl + 'user/login', { username, password })
            .pipe(
                tap((response) => {
                    this.isAuthenticatedSubject.next(true);
                    this.usernameSubject.next(response.username);
                    localStorage.setItem('authToken', response.token);
                    localStorage.setItem('username', response.username);
                })
            );
    }

    // The logout() method sends a POST request to the server to log out a user.
    public logout(): void {
        this.isAuthenticatedSubject.next(false);
        this.usernameSubject.next('');
        localStorage.removeItem('authToken');
        localStorage.removeItem('username');
        this.showSnackbar('You have been logged out', 'success-snackbar');
        this.router.navigate(['/login']);
    }

    // The showSnackbar() method displays a snackbar message to the user.
    public showSnackbar(message: string, styling: string, duration: number = 3000) {
        this.snackBar.open(message, 'OK', {
            duration: duration,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: styling,
        });
    }

    // The checkLocalStoragetoken() method checks if a token is stored in the browser's local storage. 
    // Keeps the session alive if the user refreshes the page. 
    private checkLocalStoragetoken(): void {
        const token = localStorage.getItem('authToken');
        if (token) {
            this.isAuthenticatedSubject.next(true);

            const username = localStorage.getItem('username');
            if (username) {
                this.usernameSubject.next(username);
            }
        }
    }
}

// The LoginResponse interface defines the structure of the response object returned by the server.
interface LoginResponse {
    token: string;
    username: string;
}
