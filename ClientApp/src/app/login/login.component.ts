import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { catchError, of, tap, delay, finalize } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
  username: string = '';
  password: string = '';
  isLoading: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
    ) { }

  onSubmit(): void {
    this.isLoading = true;
    this.authService.login(this.username, this.password)
    .pipe(
      tap(response => {
        console.log(`User ${this.username} logged in!`, response);
        this.authService.showSnackbar(`Hi ${this.username}! Welcome to MovieTracker!`, 'success-snackbar');
        this.router.navigate(['/']);
      }),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        const errorMessage = error.error.message;
        this.authService.showSnackbar(errorMessage, 'error-snackbar');
        return of(null); // Handle the error and return an Observable that won't break the pipeline
      }),
      finalize(() => this.isLoading = false)
    )
    .subscribe();
  }
}

