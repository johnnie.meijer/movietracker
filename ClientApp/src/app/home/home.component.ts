import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  username: string | null = null;

  constructor(public authService: AuthService) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.username = localStorage.getItem('username');
    }
  }
}
