import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddRatingDialogComponent } from '../add-rating-dialog/add-rating-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '../services/auth.service';
import { catchError, tap } from 'rxjs';
import { of } from 'rxjs';
import { MovieDetailsDialogComponent } from '../movie-details-dialog/movie-details-dialog.component';

@Component({
  selector: 'app-movie-library',
  templateUrl: './movie-library.component.html',
  styleUrls: ['./movie-library.component.css']
})

// Movie Library Component
export class MovieLibraryComponent implements OnInit{
  imageWidth = 200;
  imageMargin = 2;
  searchQuery = '';
  viewMode = 'grid';

  public watchedFilter: 'all' | 'watched' | 'unwatched' = 'all';
  private _listFilter: string = '';

  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredMovies = this.performFilter(value);
  }

  public movies: NgMovie[] = [];
  public filteredMovies: NgMovie[] = [];

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string,
    private snackBar: MatSnackBar,
    private matDialog: MatDialog,
    public authService: AuthService,
  ) { }

  ngOnInit() {
    const username = localStorage.getItem('username');
   
    this.http.get<NgMovie[]>(`${this.baseUrl}movie/userLibrary?username=${username}`).subscribe((data: any[]) => {
      this.movies = data;
      this.filteredMovies = this.movies;
    })
    console.log('Initial movies from library are set.');
  }

  toggleView() {
    this.viewMode = this.viewMode === 'table' ? 'grid' : 'table';
  }

  // Toggle Watched Status Of Movie
  toggleWatched(movie: NgMovie) {
    movie.watched = !movie.watched;
    this.updateWatchedStatus(movie, movie.watched);
  }
  
  // Perform Filter On Movies
  performFilter(filterBy: string): NgMovie[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.movies.filter((movie: NgMovie) => {
      const titleMatches = movie.title.toLocaleLowerCase().includes(filterBy);
      const watchedMatches = this.watchedFilter === 'all'
      || (this.watchedFilter == 'watched' && movie.watched)
      || (this.watchedFilter == 'unwatched' && !movie.watched);

      return titleMatches && watchedMatches;
    });      
  }

  // Set Watched Filter For Movies
  setWatchedFilter(filter: 'all' | 'watched' | 'unwatched') {
    this.watchedFilter = filter;
    this.filteredMovies = this.performFilter(this.listFilter);
  }

  // Update Watched Status Of Movie
  updateWatchedStatus(movie: NgMovie, watched: boolean) {
    const username = localStorage.getItem('username');
  
    this.http.put(`${this.baseUrl}movie/${username}/library/${movie.id}/watched?watched=${watched}`, {})
      .subscribe(
        () => {
          console.log(`${movie.title} watched status updated to ${watched}`);
          this.showSnackbar(`${movie.title} watched status updated`, 'success-snackbar');
        },
        error => {
          console.error(error);
          console.error(`Could not update ${movie.title} watched status.`);
          this.showSnackbar(`${movie.title} watched status could not be updated`, 'error-snackbar')
        }
      );
  }

  // Delete Movie From Library
  deleteMovie(movie: NgMovie) {
    const username = localStorage.getItem('username');

    this.http.delete(`${this.baseUrl}movie/${username}/library/${movie.id}`).subscribe(
      () => {
        console.log(`${movie.title.toLocaleUpperCase()} deleted from user library`);
        this.showSnackbar(`${movie.title.toLocaleUpperCase()} deleted from your library`, 'success-snackbar');
        this.movies = this.movies.filter(m => m.id !== movie.id);
        this.filteredMovies = this.movies;
      },
      error => {
        console.error(error);
        console.error(`Could not delete ${movie.title.toLocaleUpperCase()} from library.`);
        this.showSnackbar(`${movie.title} could not be deleted from library`, 'error-snackbar')
      }
    )
  }

  // Add Rating To Movie
  addRating(movie: NgMovie, rating: number) {
    const username = localStorage.getItem('username');

    this.http.put(`${this.baseUrl}movie/${username}/library/${movie.id}?personalRating=${rating}`, {})
      .pipe(
        tap(() => {
          console.log(`${movie.title} rating updated to ${rating}`);
          this.showSnackbar(`${movie.title} rating updated to ${rating}`, 'success-snackbar');
          movie.personalRating = rating;
        }),
        catchError(err => {
          console.error(err);
          this.showSnackbar(`${movie.title} rating could not be updated`, 'error-snackbar');
          return of(null);
        })
      )
      .subscribe();
  }

  // Open Dialog To Add Rating
  openRatingDialog(movie: NgMovie) {
    const dialogRef: MatDialogRef<AddRatingDialogComponent> = this.matDialog.open(AddRatingDialogComponent, {
      width: '380px',
      data: { movieTitle: movie.title }
    });

    dialogRef.afterClosed().subscribe((rating) => {
      if (rating !== null && rating !== undefined) {
        this.addRating(movie, rating);
      }
    });
  }

  // Open Dialog To view Movie Details
  openDetailsDialog(movie: NgMovie) {
    const dialogRef: MatDialogRef<MovieDetailsDialogComponent> = this.matDialog.open(MovieDetailsDialogComponent, {
      width: '900px',
      data: movie // This passes the movie object directy to the dialog component
    });
  }

  // Show Snackbar Message
  showSnackbar(message: string, styling: string) {
    this.snackBar.open(message, 'OK', {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: styling,
    });
  }
}

// Movie Interface
export interface NgMovie {
  title: string;
  id: number;
  posterUrl: string;
  tmdbRating: number;
  personalRating: number;
  director: string;
  description: string;
  release: number;
  createdAt: Date;
  updatedAt: Date;
  watched: boolean;
  cast: CastMember[];
  genres: Genre[];
}

// CastMember Interface
export interface CastMember {
  id: number;
  character: string;
  actorName: string;
}

// Genre Interface
export interface Genre {
  id: number;
  name: string;
}
