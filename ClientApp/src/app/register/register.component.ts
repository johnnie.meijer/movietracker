import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, finalize, tap } from 'rxjs';
import { of } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  form: FormGroup;
  errorArray: string = '';
  isLoading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {
      validators: this.passwordsMatch
    });
  }

  passwordsMatch(form: FormGroup): { [key: string]: boolean } | null {
    const password = form.get('password');
    const confirmPassword = form.get('confirmPassword');
    return (password && confirmPassword && password.value === confirmPassword.value) ? null : { 'mismatch': true };
  }

  onSubmit(): void {
    this.isLoading = true; // Show loading spinner
    if (this.form?.invalid) {
      this.authService.showSnackbar('Please fill out all fields', 'error-snackbar');
      return;
    }

    const user = this.form.value;

    if (user.password !== user.confirmPassword) {
      this.authService.showSnackbar('Passwords do not match', 'error-snackbar');
      return;
    }

    this.authService.register(user)
      .pipe(
        tap(response => {
          console.log(`User ${user.username} registered!`, response);
          this.authService.showSnackbar(`Successfully registered! Please login to use your account.`, 'success-snackbar');
          this.router.navigate(['/login']);
        }),
        catchError((error: HttpErrorResponse) => {
          console.error(error);

          if (Array.isArray(error.error)) {
            error.error.forEach((err: any) => {
              this.errorArray += err.description + '\n';
            });
            this.authService.showSnackbar(this.errorArray, 'error-snackbar', 10000);
            this.errorArray = '';
          } else {
            const errorMessage = error.error || 'An unknown error occurred';
            this.authService.showSnackbar(errorMessage, 'error-snackbar');
          }

          return of(null); // Handle the error and return an Observable that won't break the pipeline
        }),
        finalize(() => this.isLoading = false)
      )
      .subscribe(); // No need to handle anything here, as everything is handled within the pipelineo handle anything here, as everything is handled within the pipeline
  }
}



