using System.ComponentModel.DataAnnotations;

namespace MovieTracker.Models;

public class RegisterViewModel
{
    [Required(ErrorMessage = "Username is required")]
    public string UserName { get; set; }
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
}