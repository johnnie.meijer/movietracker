using MovieTracker;

// Model for the joining table, creates a many-to-many relationship between User and Movie models. 
public class UserMovie
{
    public string UserId { get; set; }

    public User User { get; set; }

    public int MovieId { get; set; }

    public Movie Movie { get; set; }

    public double PersonalRating { get; set; }

    public bool Watched { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset UpdatedAt { get; set; }
}