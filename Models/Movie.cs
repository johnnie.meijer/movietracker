namespace MovieTracker; 

public class Movie
{
    public Movie()
    {
        UserMovies = new List<UserMovie>();
        Cast = new List<CastMember>();   
        MovieGenres = new List<MovieGenre>(); 
    }

    public string? Title {get; set;}

    public int Id {get; set;}

    public string? PosterUrl {get; set;}

    public double TmdbRating {get; set;}

    public string? Description {get; set;}
    
    public string? Director {get; set;}

    public int? Release {get; set;}

    public DateTimeOffset CreatedAt {get; set;}

    public DateTimeOffset UpdatedAt {get; set;}

    public virtual ICollection<UserMovie> UserMovies { get; set; }

    public virtual ICollection<CastMember> Cast { get; set; }

    public virtual ICollection<MovieGenre> MovieGenres { get; set; }
}