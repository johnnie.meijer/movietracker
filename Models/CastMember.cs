using MovieTracker;

public class CastMember 
{
    public int Id { get; set; }
    public string? Character { get; set; }
    public string? ActorName { get; set; }
    public int MovieId { get; set; }
    public Movie Movie { get; set; }
}