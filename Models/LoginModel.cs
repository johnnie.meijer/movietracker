using System.ComponentModel.DataAnnotations;

// The model used for logging in. Contains only the username and password.
// This is used in the UserController.
public class LoginModel { 

    [Required]
    public string UserName { get; set; } 

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; } 
}