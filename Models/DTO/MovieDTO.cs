public class MovieDTO
{
    public string? Title { get; set; }
    public int Id { get; set; }
    public string? PosterUrl { get; set; }
    public double TmdbRating { get; set; }
    public double PersonalRating { get; set; }
    public string? Director { get; set; }
    public bool Watched { get; set; }
    public string? Description { get; set; }
    public int? Release { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset UpdatedAt { get; set; }
    public ICollection<CastMemberDTO>? Cast { get; set; }
    public ICollection<GenreDTO>? Genres { get; set; }
}