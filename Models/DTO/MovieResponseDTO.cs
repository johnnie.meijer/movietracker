public class MovieResponseDTO
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Director { get; set; }
    public int? Release { get; set; }
}