public class CastMemberDTO
{
    public int Id { get; set; }
    public string? Character { get; set; }
    public string? ActorName { get; set; }
    
}