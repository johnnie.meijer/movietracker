using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace MovieTracker;

public class User : IdentityUser
{
    [StringLength(50, MinimumLength = 2)]
    public string? FirstName { get; set; }
    
    [StringLength(50, MinimumLength = 2)]
    public string? LastName { get; set; }

    [DataType(DataType.Date)]
    public DateTime DateOfBirth { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public DateTimeOffset UpdatedAt { get; set; }

    public virtual ICollection<UserMovie> UserMovies { get; set; }
}