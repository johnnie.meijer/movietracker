using MovieTracker;

public class UserMovieRequest {

    public Movie? Movie { get; set; }
    public string? UserName { get; set; }
}